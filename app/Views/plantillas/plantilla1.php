<!DOCTYPE html>
<html lang="en" style="height: auto;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AdminLTE 3 | Blank Page</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
        <link rel="stylesheet" href="assets/css/font-awesome/all.min.css">
        <link rel="stylesheet" href="assets/css/adminlte.min.css">
        <link rel="stylesheet" href="assets/css/restaurante.css"><!-- css propio de mi web -->
        
        
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Dancing+Script&display=swap" rel="stylesheet">
    </head>
    
    <body class="sidebar-mini" style="height: auto;">

        <div class="wrapper">

            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <?= $this->include('common/navbar') ?><!-- Navbar -->
            </nav>

            <aside class="main-sidebar sidebar-dark-primary elevation-4" 
                   style="position:fixed;"> <!-- overflow-x: hidden; width: 0; -->
                <?= $this->include('common/sidebar') ?><!-- Sidebar -->
            </aside>

            <div class="content-wrapper" style="min-height: 1604.8px;">

                <section class="content-header">
                    <div class="container-fluid">
                        <h1><?= $this->renderSection('page_title') ?></h1>
                    </div>
                </section>
                    
                <section class="content">
                        <div class="mx-4 p-3 rounded-4"> 
                           <?= $this->renderSection('content') ?>
                        </div>                                           
                </section>

            </div>

            <footer class="main-footer">
                <?= $this->include('common/footer') ?><!-- Footer, pie de página -->
            </footer>

        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/adminlte.min.js"></script>
    </body>
</html>

