<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="mt-5" style="font-family: 'Dancing Script', cursive; font-size: 17px;">
      <div class="d-flex justify-content-around flex-wrap">
        <!-- Entrante -->
        <div id="platos" class="text-center align-self-center border border-dark rounded-4 mb-5 mx-4 p-5">
          <h1 class="mt-3" style="font-size: 40px;">Entrantes</h1>
          <dl>
            <dd>
              <p>Ensalada fresca de la huerta</p>
            </dd>
            <dd>
              <p>
                  Sopa de Verduras de Temporada
              </p>
            </dd>
          </dl>
          
          <div class="mb-4">
            <a href="apartado_descubre/hiphop/hiphop.html">
              <button class="btn btn-info" type="button" id="mas_platos"> Ver con detalle </button>
            </a>
          </div>
        </div>

        <!-- Principal -->
        <div id="platos" class="text-center align-self-center border border-5 border-dark rounded-4 mb-5 mx-4 p-3">
          <h1 class="mt-3" style="font-size: 40px;">Principal</h1>
          <dl>
            <dd>
              <p>
                Pollo a la parrilla con Hierbas Frescas y puré de patatas
              </p>
            </dd>
            <dd>
              <p>
                Pasta con Salsa de Tomate y Albóndigas
              </p>
            </dd>
            <dd>
              <p>
                Salmón al Horno con Ensalada de Quinoa
              </p>
            </dd>
            <dd>
              <p>
                Patatas Asadas con Romero y Zanahorias Glaseadas
              </p>
            </dd>
            <dd>
              <p>
                Pasta con Espinacas, Salsa de Queso y Champiñones
              </p>
            </dd>
          </dl>
          
          <div class="mb-4">
            <a href="apartado_descubre/latino/latino.html">
              <button class="btn btn-info" type="button" id="mas_platos"> Ver con detalle </button>
            </a>
          </div>
        </div>

        <!-- Postres -->
        <div id="platos" class="text-center align-self-center border border-5 border-dark rounded-4 mb-5 mx-4 p-5">
          <h1 class="mt-3" style="font-size: 40px;">Postres</h1>
          <dl>
            <dd>
              <p>
                Frutas Frescas con Yogur Natural
              </p>
            </dd>
            <dd>
              <p>
                Tarta de Manzana Casera
              </p>
            </dd>
          </dl>
          
          <div class="mb-4">    
            <a href="apartado_descubre/ópera/opera.html">
              <button class="btn btn-info" type="button" id="mas_platos"> Ver con detalle </button>
            </a>
          </div>
        </div>

        <!-- Bebida -->
        <div id="platos" class="text-center align-self-center border border-5 border-dark rounded-4 mb-5 mx-4 p-3 px-5">
          <h1 class="mt-3" style="font-size: 40px;">Bebida</h1>
          <dl>
            <dd>
              <p>
                Limonada Refrescante
              </p>
            </dd>
            <dd>
              <p>
                Té Verde
              </p>
            </dd>
            <dd>
              <p>
                Café Americano
              </p>
            </dd>
            <dd>
              <p>
                Café espreso
              </p>
            </dd>
            <dd>
              <p>
                Café con Leche
              </p>
            </dd>
            <dd>
              <p>
                Botella de Agua de 500 mL 
              </p>
            </dd>
          </dl>
          
          <div class="mb-4">    
            <a href="">
              <button class="btn btn-info" type="button" id="mas_platos"> Ver con detalle </button>
            </a>
          </div>
        </div>

      </div>
    </div>

<?= $this->endSection() ?>


