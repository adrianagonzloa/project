<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div>
        <h2> Bienvenido </h2>
        <h3> En este restaurante vas a tener una experiencia unica con las creaciones culinarias que encontrarás en nuestro restaurante </h3>
    </div>
<?= $this->endSection() ?>

