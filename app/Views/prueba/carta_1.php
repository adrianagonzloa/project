<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/carta') ?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<dl class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

    <dt>Entrantes</dt>
    <dd>
        <p>Ensalada fresca de la huerta</p>
    </dd>
    <dd>
        <p>
            Sopa de Verduras de Temporada
        </p>
    </dd>
    <dt>Principales</dt>
    <dd>
        <p>
            Pollo a la parrilla con Hierbas Frescas y puré de patatas
        </p>
    </dd>
    <dd>
        <p>
            Pasta con Salsa de Tomate y Albóndigas
        </p>
    </dd>
    <dd>
        <p>
            Salmón al Horno con Ensalada de Quinoa
        </p>
    </dd>
    <dd>
        <p>
            Patatas Asadas con Romero y Zanahorias Glaseadas
        </p>
    </dd>
    <dd>
        <p>
            Pasta con Espinacas, Salsa de Queso y Champiñones
        </p>
    </dd>
    <dt>Postres</dt>
    <dd>
        <p>
            Frutas Frescas con Yogur Natural
        </p>
    </dd>
    <dd>
        <p>
            Tarta de Manzana Casera
        </p>
    </dd>
    <dt>Bebidas</dt>
    <dd>
        <p>
            Limonada Refrescante
        </p>
    </dd>
    <dd>
        <p>
            Té Verde
        </p>
    </dd>
    <dd>
        <p>
            Café Americano
        </p>
    </dd>
    <dd>
        <p>
            Café espreso
        </p>
    </dd>
    <dd>
        <p>
            Café con Leche
        </p>
    </dd>
    <dd>
        <p>
            Botella de Agua de 500 mL 
        </p>
    </dd>

</dl>
<?= $this->endSection() ?>


