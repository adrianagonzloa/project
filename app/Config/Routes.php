<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get('/prueba', 'Prueba::inicio');
$routes->get('/adminlte', 'Prueba::adminlte');
$routes->get('/contacto', 'Prueba::contacto');
$routes->get('/carta', 'Prueba::carta');
$routes->get('/cartas', 'Prueba::carta2');
$routes->get('/reserva', 'Prueba::reserva');
