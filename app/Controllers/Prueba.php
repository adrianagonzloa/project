<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of Prueba
 *
 * @author a026897340j
 */
class Prueba extends BaseController{
    
    public function inicio(){
        $hoy = date('l d-m-Y, g:i a');
        echo "Buenas tardes";
        echo '<br>';
        
        echo "Hoy es $hoy";
        echo '<br>';
        
        echo "Zona horaria Mountain Standard Time (MST)";
         
    }

    public function adminlte(){
        $data['title'] = 'Bienvenido a la página de nuestro restaurante';
            return view('prueba/ejemplo', $data);
    }
    
    
    public function contacto(){
        $data['title'] = 'Esto es un ejemplo de Contacto';
            return view('prueba/contacto', $data);
    }
    
    public function carta(){
        $data['title'] = 'Esta es nuestra deliciosa carta';
            return view('prueba/carta', $data);
    }
    
    public function carta2(){
        $data['title'] = 'Esta es nuestra deliciosa carta';
            return view('prueba/carta_1', $data);
    }
    
    public function reserva(){
        $data['title'] = 'Haz una reserva!';
            return view('prueba/reservas', $data);
    }


    
}
